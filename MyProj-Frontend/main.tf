provider "google" {
  access_token = "4/0AWtgzh54iUEDHCkFn5cZJtoyxKIWS71S4jVFOs_UhSq18Bk5FaOR53sQJCwmi7eI6tf2IQ"
  project = "epitech-project-376214"
  region  = "europe-west1"
}

resource "google_compute_instance" "frontend" {
  name         = "frontend"
  machine_type = "n1-standard-1"
  zone         = "europe-west1"
  boot_disk {
    initialize_params {
      image = "ubuntu-202101-20210105"
    }
  }
  network_interface {
    network = "default"
    access_config {}
  }
}

resource "google_compute_forwarding_rule" "lb_frontend" {
  name       = "frontend-lb"
  port_range = "80"
  target     = google_compute_target_pool.frontend_tp.self_link
}

resource "google_compute_target_pool" "frontend_tp" {
  name = "frontend-target-pool"
  instances = [
    google_compute_instance.frontend.self_link
  ]
}