variable "project_id" {
  type        = string
  description = "epitech-project-376214"
}

variable "region" {
  type        = string
  description = "europe-west1"
}

provider "google" {
  project = var.project_id
  region  = var.region
}

resource "google_compute_instance" "backend" {
  name         = "backend"
  machine_type = "n1-standard-1"
  network_interface {
    network = "default"
    access_config {}
  }
  boot_disk {
    initialize_params {
      image = "ubuntu-202101-20210105"
    }
  }
}

resource "google_compute_health_check" "frontend_hc" {
  name               = "frontend-health-check"
  tcp_health_check {
    port = "80"
  }
}