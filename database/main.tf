variable "project_id" {
  type        = string
  description = "epitech-project-376214"
}

variable "region" {
  type        = string
  description = "europe-west1"
}

provider "google" {
  project = var.project_id
  region  = var.region
}

resource "google_compute_instance" "database" {
  name         = "database"
  machine_type = "n1-standard-1"
  boot_disk {
    initialize_params {
      image = "ubuntu-202101-20210105"
    }
  }
}

resource "google_sql_database_instance" "db" {
  name = "database"
  database_version = "POSTGRES_11"
  region = var.region
  settings {
    tier = "db-f1-micro"
    database_flags {
      name = "max_connections"
      value = "100"
    }
  }
}

resource "google_compute_instance" "backend" {
  name         = "backend"
  machine_type = "n1-standard-1"
  boot_disk {
    initialize_params {
      image = "ubuntu-202101-20210105"
    }
  }
  metadata {
    "DB_CONNECTION_STRING" = "postgres://user:password@${google_sql_database_instance.db.ip_address}:5432/dbname"
  }
}